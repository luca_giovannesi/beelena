import cv2
from lib.remote_inference import RemoteInference
from lib.server_frame import ServerFrame
from lib.utils import resize_image_with_aspect_ratio
import asyncio

vid = cv2.VideoCapture("app/test.mp4")
ri = RemoteInference("127.0.0.1", 5000)

def get_frame() -> cv2.Mat:
    ret, frame = vid.read()
    if ret:
        frame = resize_image_with_aspect_ratio(frame,640)
        frame = ri.Inference(frame)
        return frame
    else:
        vid.set(cv2.CAP_PROP_POS_FRAMES, 0)
        return get_frame()


sf = ServerFrame(5001, get_frame)
asyncio.run(sf.serve())
