FROM balenalib/jetson-nano-node:12.22-stretch
# FROM balenalib/raspberrypi3-debian:stretch-20190612

RUN sed -i 's/r32.6 main/r32.7 main/g' /etc/apt/sources.list.d/nvidia.list

RUN apt update && apt-get install -y wget tar lbzip2 \
  nvidia-l4t-gstreamer \
  libgstreamer1.0-0 gstreamer1.0-plugins-base \
  gobject-introspection gstreamer1.0-plugins-good \
  gstreamer1.0-plugins-bad \
  gstreamer1.0-plugins-ugly \
  gstreamer1.0-tools

RUN wget https://developer.nvidia.com/downloads/remetpack-463r32releasev73t210jetson-210linur3273aarch64tbz2 -O jetson-210_linux_r32.7.3_aarch64.tbz2  && \
  tar xf jetson-210_linux_r32.7.3_aarch64.tbz2 && \
  cd Linux_for_Tegra && \
  sed -i 's/config.tbz2\"/config.tbz2\" --exclude=etc\/hosts --exclude=etc\/hostname/g' apply_binaries.sh && \
  sed -i 's/install --owner=root --group=root \"${QEMU_BIN}\" \"${L4T_ROOTFS_DIR}\/usr\/bin\/\"/#install --owner=root --group=root \"${QEMU_BIN}\" \"${L4T_ROOTFS_DIR}\/usr\/bin\/\"/g' nv_tegra/nv-apply-debs.sh && \
  sed -i 's/chroot . \//  /g' nv_tegra/nv-apply-debs.sh && \
  ./apply_binaries.sh -r / --target-overlay && cd .. \
  rm -rf jetson-210_linux_r32.7.3_aarch64.tbz2 && \
  rm -rf Linux_for_Tegra && \
  echo "/usr/lib/aarch64-linux-gnu/tegra" > /etc/ld.so.conf.d/nvidia-tegra.conf && ldconfig


# Install dependencies
RUN apt-get update && \
  apt-get install -yq \
    python3 \
    python3-dev \
    python3-pip \
    python3-setuptools \
    gstreamer-1.0 \
    v4l-utils \
    libopus-dev \
    libvpx-dev \
    libsrtp2-dev \
    libopencv-dev \
    libatlas3-base \
    libatlas-base-dev \
    libjasper-dev \
    libilmbase12 \
    libopenexr22 \
    libavformat-dev \
    libswscale-dev \
    libqtgui4 \
    libqt4-test \
    libavdevice-dev \
    libavfilter-dev \
    libavcodec-dev \
  && apt-get clean && rm -rf /var/lib/apt/lists/*

# Enable the v4l2 driver for the Raspberry Pi camera
#RUN printf "bcm2835-v4l2\n" >> /etc/modules
RUN pip3 install --upgrade pip 
RUN pip3 install async-timeout av==6.1.0
RUN pip3 install pyee==8.2.2 aiohttp aiohttp_basicauth==0.1.3 aioice==0.6.10 aiortc==0.9.11 numpy==1.15.4 opencv-python==3.4.4.19 --index-url https://www.piwheels.org/simple

WORKDIR /usr/src/app

COPY ./app/ /usr/src/app/

CMD ["python3", "/usr/src/app/server.py"]
