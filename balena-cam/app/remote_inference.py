import cv2
import numpy as np
from websockets.sync.client import connect


class RemoteInference:
    def __init__(self, host: str, port: int):
        self.socket = connect("ws://" + host + ":" + str(port))

    def Inference(self, img):
        encoded = self.encode(img)
        self.socket.send(encoded)
        data = self.socket.recv()
        return self.decode(data)

    def encode(self, img):
        img_encoded = cv2.imencode('.jpg', img)[1]
        return img_encoded.tobytes()

    def decode(self, img_bytes):
        nparr = np.frombuffer(img_bytes, np.uint8)
        return cv2.imdecode(nparr, cv2.IMREAD_COLOR)
