#!/bin/bash

# debug mode, to set in balena device env vars
# export OPENCV_LOG_LEVEL=debug
# export OPENCV_VIDEOIO_DEBUG=1

# start the x server
nohup startx &

# start the nvargus daemon
nohup nvargus-daemon &

# start webserver
/root/.asdf/installs/python/3.11.1/bin/python3 /usr/src/app/server.py