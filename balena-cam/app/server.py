import asyncio, os, cv2, platform, sys
from time import sleep
from lib.remote_inference import RemoteInference
from lib.server_frame import ServerFrame
from lib.utils import resize_image_with_aspect_ratio


# from aiohttp_basicauth import BasicAuthMiddleware

ENV_CAMERA_DRIVER = "CAM_CAMERA_DRIVER"  # or nvargus
ENV_CAMERA_DEVICE_ID = "CAM_CAMERA_ID"
ENV_CAMERA_DRIVER_VALUE_V4L2 = "v4l2"
ENV_CAMERA_DRIVER_VALUE_NVARGUS = "nvargus"

flip = False
ri = RemoteInference("127.0.0.1", 5000)


class CameraDevice:
    def __init__(self):
        self.cap = None

        while self.cap == None:
            self.cap = self.get_video_capture()
            sleep(5)

        print("[CameraDevice] ready!")

        # ret, frame = self.cap.read()
        # if not ret:
        #     print(f'Failed to open default camera ({ret}). Exiting...')
        #     sys.exit()
        # self.cap.set(3, 640)
        # self.cap.set(4, 480)

    def get_video_capture(self):
        try:
            # cap = cv2.VideoCapture(1)
            # cap = cv2.VideoCapture(CameraDevice.gstreamer_pipeline(flip_method=0), cv2.CAP_GSTREAMER)
            # cap = cv2.VideoCapture('nvarguscamerasrc ! video/x-raw(memory:NVMM), width=640, height=480, format=(string)NV12, framerate=(fraction)20/1 ! nvvidconv ! video/x-raw, format=(string)BGRx ! videoconvert ! video/x-raw, format=(string)BGR ! appsink' , cv2.CAP_GSTREAMER)
            # cap = cv2.VideoCapture('nvarguscamerasrc ! video/x-raw(memory:NVMM), width=(int)1280, height=(int)720, format=(string)NV12, framerate=(fraction)10/1 ! nvvidconv flip-method=2 ! video/x-raw, width=(int)1280, height=(int)720, format=(string)BGRx ! videoconvert ! video/x-raw, format=(string)BGR ! appsink', cv2.CAP_GSTREAMER)
            # cap = cv2.VideoCapture(
            #     "nvarguscamerasrc sensor-id=0 ! video/x-raw(memory:NVMM), width=(int)1920, height=(int)1080, format=(string)NV12, framerate=(fraction)30/1 ! nvvidconv flip-method=2 ! video/x-raw, width=(int)1920, height=(int)1080, format=(string)BGRx ! videoconvert ! video/x-raw, format=(string)BGR ! appsink",
            #     cv2.CAP_GSTREAMER,
            # )

            # pipeline = CameraDevice.pipeline_gstreamer(
            #     sensor_id=0,
            #     capture_width=3264,  # 3264,
            #     capture_height=1848,  # 2464,
            #     display_width=3264,  # 3264,
            #     display_height=1848,  # 2464,
            #     framerate=28,
            # )

            # pipeline = CameraDevice.pipeline_v4l2(
            #     sensor_path="/dev/video1",
            #     capture_width=1920,  # 3264,
            #     capture_height=1080,  # 2464,
            #     display_width=1920,  # 3264,
            #     display_height=1080,  # 2464,
            #     framerate=30,
            # )

            # print(f"[CameraDevice] using pipeline={pipeline}")

            # cap = cv2.VideoCapture(
            #     pipeline,
            #     cv2.CAP_GSTREAMER,
            # )

            cap = None

            if ENV_CAMERA_DRIVER in os.environ:
                if ENV_CAMERA_DRIVER == ENV_CAMERA_DRIVER_VALUE_V4L2:
                    cap = cv2.VideoCapture(int(ENV_CAMERA_DEVICE_ID))
                elif ENV_CAMERA_DRIVER == ENV_CAMERA_DRIVER_VALUE_NVARGUS:
                    pipeline = CameraDevice.pipeline_gstreamer(
                        sensor_id=int(ENV_CAMERA_DEVICE_ID),
                        capture_width=1920,  # 3264,
                        capture_height=1080,  # 2464,
                        display_width=1920,  # 3264,
                        display_height=1080,  # 2464,
                        framerate=30,
                    )
                    cap = cv2.VideoCapture(
                        pipeline,
                        cv2.CAP_GSTREAMER,
                    )
                else:
                    cap = cv2.VideoCapture(0)
            else:
                cap = cv2.VideoCapture(0)

            if not cap.isOpened():
                print(f"Failed to open default camera")
                return None

            ret, frame = cap.read()
            if not ret:
                print(f"Failed to get frame from camera ({ret})")
                return None
            return cap
        except Exception as e:
            print(f"Failed to open default camera ({e})")
            return None

    def rotate(self, frame):
        if flip:
            (h, w) = frame.shape[:2]
            center = (w / 2, h / 2)
            M = cv2.getRotationMatrix2D(center, 180, 1.0)
            frame = cv2.warpAffine(frame, M, (w, h))
        return frame

    async def get_latest_frame(self):
        # print(f"[CameraDevice] get_latest_frame: called, self.cap={self.cap}")
        ret, frame = self.cap.read()
        # await asyncio.sleep(0)
        return self.rotate(frame)

    async def get_jpeg_frame(self):
        encode_param = (int(cv2.IMWRITE_JPEG_QUALITY), 100)
        frame = await self.get_latest_frame()
        frame, encimg = cv2.imencode(".jpg", frame, encode_param)
        return encimg.tostring()

    @staticmethod
    def pipeline_v4l2(
            sensor_path="/dev/video0",
            capture_width=1280,
            capture_height=720,
            display_width=1280,
            display_height=720,
            framerate=30,
            flip_method=2,
    ):
        return (
            f"v4l2src device={sensor_path}"
            f" ! video/x-raw, framerate={framerate}/1, width={capture_width}, height={capture_height}"
            f" ! videoconvert"
            f" ! appsink"
        )

    @staticmethod
    def pipeline_gstreamer(
            sensor_id=0,
            capture_width=1280,
            capture_height=720,
            display_width=1280,
            display_height=720,
            framerate=30,
            flip_method=2,
    ):
        return (
            f"nvarguscamerasrc sensor-id={sensor_id}"
            f" ! video/x-raw(memory:NVMM), width=(int){capture_width}, height=(int){capture_height}, format=(string)NV12, framerate=(fraction){framerate}/1"
            f" ! nvvidconv flip-method={flip_method}"
            f" ! video/x-raw, width=(int){display_width}, height=(int){display_height}, format=(string)BGRx"
            f" ! videoconvert ! video/x-raw, format=(string)BGR"
            f" ! appsink"
        )


def checkDeviceReadiness():
    # print debug
    os.system("v4l2-ctl --list-devices")

    if not os.path.exists("/dev/video0") and platform.system() == "Linux":
        print("Video device is not ready")
        # print('Trying to load bcm2835-v4l2 driver...')
        # os.system('bash -c "modprobe bcm2835-v4l2"')
        sleep(1)
        sys.exit()
    else:
        os.system("v4l2-ctl -d /dev/video0 --list-formats")
        os.system("v4l2-ctl -d /dev/video0 --list-formats-ext")
        print("Video device is ready")


if __name__ == "__main__":
    checkDeviceReadiness()
    camera_device = CameraDevice()


    async def generate_frame():
        frame = await camera_device.get_latest_frame()
        frame = resize_image_with_aspect_ratio(frame,640)
        frame = ri.Inference(frame)
        return frame


    sf = ServerFrame(5001, generate_frame)
    asyncio.run(sf.serve())
