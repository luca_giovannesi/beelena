# FROM balenalib/raspberrypi3-debian:stretch-20190612

# from https://raw.githubusercontent.com/balena-io-examples/jetson-examples/master/jetson-nano/Dockerfile
FROM balenalib/jetson-nano-ubuntu-python:bionic

# Prevent apt-get prompting for input
ENV DEBIAN_FRONTEND noninteractive
ENV TZ Europe/Rome

COPY *.sh /tmp/
RUN /bin/bash /tmp/install.sh
RUN /bin/bash /tmp/install_bsp.sh

# Update to 32.7 repository in case the base image is using 32.6
# RUN sed -i 's/r32.6 main/r32.7 main/g' /etc/apt/sources.list.d/nvidia.list

# RUN apt update && apt-get install -y wget tar lbzip2 \
#   nvidia-l4t-gstreamer \
#   libgstreamer1.0-0 gstreamer1.0-plugins-base \
#   gobject-introspection gstreamer1.0-plugins-good \
#   gstreamer1.0-plugins-bad \
#   gstreamer1.0-plugins-ugly \
#   gstreamer1.0-tools

# RUN wget https://developer.nvidia.com/downloads/remetpack-463r32releasev73t210jetson-210linur3273aarch64tbz2 -O jetson-210_linux_r32.7.3_aarch64.tbz2  && \
#   tar xf jetson-210_linux_r32.7.3_aarch64.tbz2 && \
#   cd Linux_for_Tegra && \
#   sed -i 's/config.tbz2\"/config.tbz2\" --exclude=etc\/hosts --exclude=etc\/hostname/g' apply_binaries.sh && \
#   sed -i 's/install --owner=root --group=root \"${QEMU_BIN}\" \"${L4T_ROOTFS_DIR}\/usr\/bin\/\"/#install --owner=root --group=root \"${QEMU_BIN}\" \"${L4T_ROOTFS_DIR}\/usr\/bin\/\"/g' nv_tegra/nv-apply-debs.sh && \
#   sed -i 's/chroot . \//  /g' nv_tegra/nv-apply-debs.sh && \
#   ./apply_binaries.sh -r / --target-overlay && cd .. \
#   rm -rf jetson-210_linux_r32.7.3_aarch64.tbz2 && \
#   rm -rf Linux_for_Tegra && \
#   echo "/usr/lib/aarch64-linux-gnu/tegra" > /etc/ld.so.conf.d/nvidia-tegra.conf && ldconfig

# RUN \
#   apt-get install -y --no-install-recommends \
#   xserver-xorg-input-evdev \
#   xinit \
#   xfce4 \
#   xfce4-terminal \
#   x11-xserver-utils \
#   dbus-x11 \
#   xterm

# ENV LD_LIBRARY_PATH=/usr/lib/aarch64-linux-gnu/tegra
# ENV XFCE_PANEL_MIGRATE_DEFAULT=1
# ENV UDEV=1

# # Prevent screen from turning off
# RUN echo "#!/bin/bash" > /etc/X11/xinit/xserverrc \
#   && echo "" >> /etc/X11/xinit/xserverrc \
#   && echo 'exec /usr/bin/X -s 0 dpms' >> /etc/X11/xinit/xserverrc

## Sample Gstreamer playback in webterminal:
## $ export DISPLAY=0.0
## $ gst-launch-1.0 filesrc location="file_example.mp4" ! qtdemux name=demux ! h264parse ! omxh264dec ! nvoverlaysink -e

##  Optional: Sample CUDA Clock sample run in webterminal:
# RUN apt install -yq nvidia-l4t-cuda nvidia-cuda libcudnn8 libcudnn8-dev
# cuda-samples-10-2 && cd /usr/local/cuda-10.2/samples/0_Simple/clock/ && make && ./clock
##  Sample output:
##  CUDA Clock sample
##  GPU Device 0: "Maxwell" with compute capability 5.3
##  Average clocks/block = 3294.203125

# Install dependencies
# RUN apt update && \
#   apt install -yq \
#   python3 \
#   python3-dev \
#   python3-pip \
#   python3-setuptools \
#   python3-venv \
#   python3-pyee \
#   gstreamer-1.0 \
#   v4l-utils \
#   python3-aiohttp \
#   python3-numpy \
#   python3-opencv \
#   libopus-dev \
#   libvpx-dev \
#   libsrtp2-dev \
#   libopencv-dev \
#   libatlas3-base \
#   libatlas-base-dev \
#   libopenexr22 \
#   libavformat-dev \
#   libswscale-dev \
#   libqtgui4 \
#   libqt4-test \
#   libavdevice-dev \
#   pkg-config \
#   libavformat-dev \
#   libavcodec-dev \
#   libavdevice-dev \
#   libavutil-dev \ 
#   libswscale-dev \ 
#   libswresample-dev \
#   libavfilter-dev

# python3-aioice \
#
# python3-av \
# python3-async-timeout \
# python3-aiortc \
# libjasper-dev \
# libilmbase12 \
# libavfilter-dev \
# libavcodec-dev \

# install some OpenCV prerequesets
# RUN apt install -yq libgtk2.0-dev libavcodec-dev libavformat-dev libswscale-dev libwebp-dev libtbb2 libtbb-dev libgstreamer1.0-0 \
#   gstreamer1.0-plugins-base gstreamer1.0-plugins-good gstreamer1.0-plugins-bad cmake pkg-config


# pyav deps
# Library components
# RUN apt-get install -y pkg-config \
#     libavformat-dev libavcodec-dev libavdevice-dev \
#     libavutil-dev libswscale-dev libswresample-dev libavfilter-dev

# RUN apt update && DEBIAN_FRONTEND=noninteractive apt install -y tzdata git build-essential libssl-dev zlib1g-dev \
#                                                                 libbz2-dev libreadline-dev libsqlite3-dev curl \
#                                                                 libncursesw5-dev xz-utils tk-dev libxml2-dev \
#                                                                 libxmlsec1-dev libffi-dev liblzma-dev


# ARG USERNAME=jetson
# ARG USER_UID=1000
# ARG USER_GID=$USER_UID

# RUN groupadd --gid $USER_GID $USERNAME \
#     && useradd --uid $USER_UID --gid $USER_GID -m $USERNAME \
#     #
#     # [Optional] Add sudo support. Omit if you don't need to install software after connecting.
#     && apt-get update \
#     && apt-get install -y sudo \
#     && echo $USERNAME ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/$USERNAME \
#     && chmod 0440 /etc/sudoers.d/$USERNAME

# USER $USERNAME
# ENV HOME=/home/$USERNAME
# ENV HOME=/root
# # # RUN sudo usermod -s /bin/bash $USERNAME

# # install asdf
# RUN git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch v0.11.1
# RUN sudo chmod +x $HOME/.asdf/asdf.sh 
# # # bash
# RUN echo '. "$HOME/.asdf/asdf.sh"' >> ~/.bashrc
# RUN echo '. "$HOME/.asdf/completions/asdf.bash"' >> ~/.bashrc
# # # sh
# RUN echo 'export ASDF_DIR="$HOME/.asdf"' >> ~/.profile
# RUN echo '. "$HOME/.asdf/asdf.sh"' >> ~/.profile

# # # install python
# RUN $HOME/.asdf/bin/asdf plugin add python
# RUN $HOME/.asdf/bin/asdf install python 3.11.1
# RUN $HOME/.asdf/bin/asdf global python 3.11.1
# RUN $HOME/.asdf/installs/python/3.11.1/bin/pip install --upgrade pip

# # # install requirements
# COPY requirements.txt /tmp/requirements.txt
# RUN $HOME/.asdf/installs/python/3.11.1/bin/pip install -r /tmp/requirements.txt

# install python
COPY requirements.txt /tmp/requirements.txt
RUN /bin/bash /tmp/install_python.sh

# install opencv
RUN /bin/bash /tmp/install_opencv.sh
# RUN rm -rfv /tmp/*.sh

# install x server
RUN \
  apt-get install -y --no-install-recommends \
  xserver-xorg-input-evdev \
  xinit \
  xfce4 \
  xfce4-terminal \
  x11-xserver-utils \
  dbus-x11 \
  xterm

ENV LD_LIBRARY_PATH=/usr/lib/aarch64-linux-gnu/tegra
ENV XFCE_PANEL_MIGRATE_DEFAULT=1
ENV UDEV=1

# Prevent screen from turning off
RUN echo "#!/bin/bash" > /etc/X11/xinit/xserverrc \
  && echo "" >> /etc/X11/xinit/xserverrc \
  && echo 'exec /usr/bin/X -s 0 dpms' >> /etc/X11/xinit/xserverrc

RUN echo "/usr/lib/aarch64-linux-gnu/tegra-egl" > /etc/ld.so.conf.d/nvidia-tegra-egl.conf && ldconfig

# clean
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

WORKDIR /usr/src/app

# Enable the v4l2 driver for the Raspberry Pi camera
#RUN printf "bcm2835-v4l2\n" >> /etc/modules
# RUN python3 -m venv env
# RUN ./env/bin/pip3 install --upgrade pip 
# RUN ./env/bin/pip3 install async-timeout av==6.1.0
# RUN ./env/bin/pip3 install pyee==8.2.2 aiohttp aiohttp_basicauth==0.1.3 aioice==0.6.10 aiortc==0.9.11 numpy==1.15.4 opencv-python==3.4.4.19 --index-url https://www.piwheels.org/simple

COPY ./app/ /usr/src/app/

# CMD ["python3", "/usr/src/app/server.py"]
# CMD ["/root/.asdf/installs/python/3.11.1/bin/python3", "/usr/src/app/server.py"]
CMD ["/bin/bash", "/usr/src/app/start.sh"]