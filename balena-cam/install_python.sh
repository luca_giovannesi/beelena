#!/bin/bash
source /tmp/common.sh

apt install -y tzdata git build-essential libssl-dev zlib1g-dev \
                libbz2-dev libreadline-dev libsqlite3-dev curl \
                libncursesw5-dev xz-utils tk-dev libxml2-dev \
                libxmlsec1-dev libffi-dev liblzma-dev

git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch v0.11.1
sudo chmod +x $HOME/.asdf/asdf.sh
# # bash
echo '. "$HOME/.asdf/asdf.sh"' >> ~/.bashrc
echo '. "$HOME/.asdf/completions/asdf.bash"' >> ~/.bashrc
# # sh
echo 'export ASDF_DIR="$HOME/.asdf"' >> ~/.profile
echo '. "$HOME/.asdf/asdf.sh"' >> ~/.profile

# # install python
$ASDF plugin add python
$ASDF install python 3.11.1
$ASDF global python 3.11.1
$PIP3 install --upgrade pip

# install requirements
$PIP3 install -r /tmp/requirements.txt