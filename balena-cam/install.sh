#!/bin/bash
source /tmp/common.sh

apt update
apt upgrade -y

sed -i 's/r32.6 main/r32.7 main/g' /etc/apt/sources.list.d/nvidia.list

# gstreamer base
apt install -y wget tar lbzip2 binutils \
                nvidia-l4t-gstreamer \
                libgstreamer1.0-0 gstreamer1.0-plugins-base \
                gobject-introspection gstreamer1.0-plugins-good \
                libgstreamer1.0-dev libgstreamer-opencv1.0-0 \
                libgstreamer-plugins-* \
                gstreamer1.0-plugins-bad \
                gstreamer1.0-plugins-ugly \
                gstreamer1.0-tools \
                apt-utils


apt install -y gstreamer1.0-tools gstreamer1.0-alsa \
            gstreamer1.0-plugins-base gstreamer1.0-plugins-good \
            gstreamer1.0-plugins-bad gstreamer1.0-plugins-ugly \
            gstreamer1.0-libav \
            gstreamer1.0-plugins-* \
            gstreamer1.0-python3-plugin-loader \
     
apt install -y libgstreamer1.0-dev \
            libgstreamer-plugins-base1.0-dev \
            libgstreamer-plugins-good1.0-dev \
            libgstreamer-plugins-bad1.0-dev \
            libgstreamer-plugins-* \
            libgstreamer-*

# cuda
# apt install -yq nvidia-l4t-cuda nvidia-cuda libcudnn8 libcudnn8-dev

# # python
#   python3 \
#   python3-dev \
#   python3-pip \
#   python3-setuptools \
#   python3-venv \
  # python3-pyee \
  # python3-aiohttp \
  # python3-numpy \
  # python3-opencv \
  # python3-urllib3 \
  # cython3 \

apt install -y \
  ffmpeg \
  gstreamer-1.0 \
  v4l-utils \
  libopus-dev \
  libvpx-dev \
  libsrtp2-dev \
  libopencv-dev \
  libatlas3-base \
  libatlas-base-dev \
  libopenexr22 \
  libavformat-dev \
  libswscale-dev \
  libqtgui4 \
  libqt4-test \
  libavdevice-dev \
  pkg-config \
  libavformat-dev \
  libavcodec-dev \
  libavdevice-dev \
  libavutil-dev \
  libswscale-dev \
  libswresample-dev \
  libavfilter-dev \
  libffi-dev

#   python3-av \

# av
apt install -y libgtk2.0-dev libavcodec-dev libavformat-dev libswscale-dev libwebp-dev libtbb2 libtbb-dev libgstreamer1.0-0 \
  gstreamer1.0-plugins-base gstreamer1.0-plugins-good gstreamer1.0-plugins-bad cmake pkg-config

# apt clean 
# rm -rf /var/lib/apt/lists/*