#!/bin/bash
source /tmp/common.sh

git clone https://github.com/opencv/opencv.git
cd opencv/
git checkout 4.1.0

mkdir build
cd build
cmake -D CMAKE_BUILD_TYPE=RELEASE \
-D INSTALL_PYTHON_EXAMPLES=ON \
-D INSTALL_C_EXAMPLES=OFF \
-D PYTHON_EXECUTABLE=$(which $PYTHON3) \
-D BUILD_opencv_python2=OFF \
-D CMAKE_INSTALL_PREFIX=$($PYTHON3 -c "import sys; print(sys.prefix)") \
-D PYTHON3_EXECUTABLE=$(which $PYTHON3) \
-D PYTHON3_INCLUDE_DIR=$($PYTHON3 -c "from distutils.sysconfig import get_python_inc; print(get_python_inc())") \
-D PYTHON3_PACKAGES_PATH=$($PYTHON3 -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())") \
-D WITH_GSTREAMER=ON \
-D BUILD_EXAMPLES=ON ..

make -j2

make install
ldconfig