FROM balenalib/jetson-nano-ubuntu-python:3.8-bionic-build

# Prevent apt-get prompting for input
ENV DEBIAN_FRONTEND noninteractive

# Update to 32.7 repository in case the base image is using 32.6
RUN sed -i 's/r32.6 main/r32.7 main/g' /etc/apt/sources.list.d/nvidia.list

RUN apt-get update && apt-get install -y wget tar lbzip2 \
    nvidia-l4t-gstreamer \
    libgstreamer1.0-0 gstreamer1.0-plugins-base \
    gobject-introspection gstreamer1.0-plugins-good \
    gstreamer1.0-plugins-bad \
    gstreamer1.0-plugins-ugly \
    gstreamer1.0-tools && \
    wget https://developer.nvidia.com/downloads/remetpack-463r32releasev73t210jetson-210linur3273aarch64tbz2 -O jetson-210_linux_r32.7.3_aarch64.tbz2  && \
    tar xf jetson-210_linux_r32.7.3_aarch64.tbz2 && \
    cd Linux_for_Tegra && \
    sed -i 's/config.tbz2\"/config.tbz2\" --exclude=etc\/hosts --exclude=etc\/hostname/g' apply_binaries.sh && \
    sed -i 's/install --owner=root --group=root \"${QEMU_BIN}\" \"${L4T_ROOTFS_DIR}\/usr\/bin\/\"/#install --owner=root --group=root \"${QEMU_BIN}\" \"${L4T_ROOTFS_DIR}\/usr\/bin\/\"/g' nv_tegra/nv-apply-debs.sh && \
    sed -i 's/chroot . \//  /g' nv_tegra/nv-apply-debs.sh && \
    ./apply_binaries.sh -r / --target-overlay && cd .. \
    rm -rf jetson-210_linux_r32.7.3_aarch64.tbz2 && \
    rm -rf Linux_for_Tegra && \
    echo "/usr/lib/aarch64-linux-gnu/tegra" > /etc/ld.so.conf.d/nvidia-tegra.conf && ldconfig

RUN apt-get install -y nvidia-l4t-cuda nvidia-cuda libcudnn8 python3.8

WORKDIR /usr/src/app
COPY . /usr/src/app

RUN python3.8 -m venv env
RUN ./env/bin/pip install --upgrade pip
RUN ./env/bin/pip install res/onnxruntime_gpu-1.6.0-cp38-cp38-linux_aarch64.whl
RUN ./env/bin/pip install -r requirements.txt

CMD ["./env/bin/python", "src/receiver.py"]
