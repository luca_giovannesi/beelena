import cv2
import numpy as np
from yolov8 import YOLOv8
import argparse
from time import time
import os

parser = argparse.ArgumentParser(prog='Yolo Tracker')
parser.add_argument('model')
parser.add_argument('filename')
args = parser.parse_args()
# Open the video file
video_path = args.filename
cap = cv2.VideoCapture(video_path)
conf_thres = os.getenv("CONF_THRES")
conf_iou = os.getenv("CONF_IOU")

yolov8_detector = YOLOv8(args.model, conf_thres=0.3, iou_thres=0.3)

top_confidentiality = 0
top_class = 0
frame_count = 0
elapsed_time_sum = 0
fps_sum = 0
while cap.isOpened():
    success, frame = cap.read()
    if success:
        start_time = time()
        boxes, scores, class_ids = yolov8_detector(frame)
        elapsed_time = time() - start_time
        for i in range(len(scores)):
            if scores[i] > top_confidentiality:
                top_confidentiality = scores[i]
                top_class = class_ids[i]
        frame_count += 1
        elapsed_time_sum += elapsed_time
        fps = 1 / elapsed_time
        fps_sum += fps
        if frame_count % 10 == 0:
            print(f"ELAPSED TIME: {elapsed_time} -> FPS: {fps}")

    else:
        break

print(f"TOP_CLASS: {top_class} with conf: {top_confidentiality}")
print(f"AVG Time: {elapsed_time_sum / frame_count}, AVG FPS: {fps_sum / frame_count}")
