# Request a frame
# import the opencv library 
import asyncio
from websockets.server import serve
import cv2
import numpy as np
from yolov8 import YOLOv8
import os

def get_env_float(name, default:float)->float:
    value = os.getenv(name)
    if value==None:
        return default
    else:
        try:
            return float(value)
        except ValueError:
            return default

conf_thres = get_env_float("CONF_THRES",0.3)
conf_iou = get_env_float("IOU_THRES",0.3)
print("Loading with Conf Thres",conf_thres, "IOU Thres",conf_iou)
yolov8_detector = YOLOv8("model.onnx", conf_thres=conf_thres, iou_thres=conf_iou)


def encode(img):
    img_encoded = cv2.imencode('.jpg', img)[1]
    return img_encoded.tobytes()


def decode(img_bytes):
    nparr = np.frombuffer(img_bytes, np.uint8)
    return cv2.imdecode(nparr, cv2.IMREAD_COLOR)


async def echo(websocket):
    async for message in websocket:
        frame = decode(message)
        yolov8_detector(frame)
        frame = yolov8_detector.draw_detections(frame, mask_alpha=0)
        encoded = encode(frame)
        await websocket.send(encoded)


async def main():
    async with serve(echo, "0.0.0.0", 5000):
        await asyncio.Future()  # run forever


asyncio.run(main())
