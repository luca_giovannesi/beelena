from typing import Union
from websockets.sync.client import connect
from fastapi import FastAPI
from fastapi.staticfiles import StaticFiles
# from fastapi.middleware.cors import CORSMiddleware

app = FastAPI()
port=5001

def get_frame():
    socket = connect("ws://127.0.0.1:" + str(port))
    socket.send(b"next")
    data = socket.recv()
    socket.close()
    return data

@app.get("/api/frame")
def read_root():
    f = get_frame()
    return f

app.mount("/", StaticFiles(directory="dist",html=True), name="static")

""" origins = [
    "http://localhost:8000",
    "http://localhost:8080",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
) """